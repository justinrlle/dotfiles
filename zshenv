function array-contain () { # $1: name of the array, $2: value
  [ "$1" = "--help" ] && {
    echo "use the following way: array-contain ARRAY_NAME VALUE"
    return 0
  }

  local arr_name="$1"
  local val="$2"
  eval "[[ \${${arr_name}[(ie)\$val]} -le \${#${arr_name}} ]]"
  return $?
}


function path-check-presence () {
  array-contain path "$1"
  return $?
}

function path-append () {
  for new_path; do
    if ! path-check-presence "$new_path"; then
      path+="$new_path"
    fi
  done
}

function path-prepend () {
  for new_path; do
    if ! path-check-presence "$new_path"; then
      path[1,0]="$new_path"
    fi
  done
}

path-prepend "$HOME/.local/bin"

export LOCAL_ZSHENV="${HOME}/.zshenv.${HOST}"
export HOSTNAME="$(hostname)"
if [ -r "$LOCAL_ZSHENV" ]; then
  source "$LOCAL_ZSHENV"
fi
if [ -r "${HOME}/.zshenv.local" ]; then
  source "${HOME}/.zshenv.local"
fi
zstyle ':omz:update' mode disabled
