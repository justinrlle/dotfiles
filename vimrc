scriptencoding utf-8
set fileencoding=utf-8
if filereadable($VIMRUNTIME . '/defaults.vim')
  source $VIMRUNTIME/defaults.vim
endif

set background=dark
let g:gruvbox_improved_strings = 0
colorscheme gruvbox-raw

set number
augroup AutoNumbers
  au!
  au InsertEnter * set norelativenumber
  au InsertLeave * set relativenumber
augroup END
set relativenumber

set shiftwidth=2
set tabstop=2
set expandtab

set scrolloff=5

augroup vimStartup
  au!
  " When editing a file, always jump to the last known cursor position.
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif
augroup END

let mapleader = ','
let g:mapleader = ','

if !isdirectory(expand('~/.vim/tmp')) 
      \ || !isdirectory(expand('~/.vim/tmp/swap_files'))
      \ || !isdirectory(expand('~/.vim/tmp/undo_files'))
  call mkdir($HOME . '/.vim/tmp/swap_files', 'p')
  call mkdir($HOME . '/.vim/tmp/undo_files', 'p')

endif

if $COLORTERM ==# 'truecolor' && has('termguicolors')
  set termguicolors
endif

set nobackup
set nowritebackup
set swapfile
set directory=~/.vim/tmp/swap_files
set undodir=~/.vim/tmp/undo_files
set undofile

set virtualedit=block
set splitbelow
set splitright

set showcmd
set ruler
set wildmenu

set smartcase
set hlsearch
set incsearch

set shiftwidth=2
set tabstop=2
set expandtab
set gdefault

set statusline=%<%f\ %y " Clear current, set troncature, print filename and filetype
set statusline+=\ \ %L\ lines
set statusline+=\ %h%m%r " Add a space, show help/modified/readonly flag
set statusline+=%= " Switch to right

" Add a group of a maximum width of 18, left justified, showing the column and
" line number, and position in file (in percentage, maybe pad it)
set statusline+=%-18.(l:\%l,\c:\ %c%)\ %P
set laststatus=2

set completeopt=longest,menuone

set listchars=tab:╺─,trail:─,extends:>,precedes:<,nbsp:+ " ,eol:╸ might be cool
set list

nnoremap <F1> <nop>
inoremap <F1> <nop>
vnoremap <F1> <nop>

nnoremap <esc><esc> :noh<return><esc>
nnoremap <leader>b :ls<return>:b<space>

nnoremap n nzz

nnoremap Y y$
nnoremap H ^
nnoremap L $
nnoremap K kJ

nnoremap p ]p
nnoremap <leader>p "+]p
vnoremap <leader>p "+]p
nnoremap <leader>yy "+yy
vnoremap <leader>y "+y
nnoremap <leader>dd "+dd
vnoremap <leader>d "+d

command! W w
command! Q q
command! E e
command! WQ wq
command! Wq wq
command! WA wa
command! Wa wa
command! QA qa
command! Qa qa
command! Wqa wqa
command! WQa wqa
command! WQA wqa

inoremap jj <Esc>


if $TERM ==# 'xterm-256color'
  let &t_EI = "\<Esc>[2 q"
  let &t_SI = "\<Esc>[5 q"
  let &t_SR = "\<Esc>[3 q"
endif

if has('gui_running')
  let &guifont = 'Fira Code Retina:h13'
  set lines=50
  set columns=150
endif

call plug#begin('~/.vim/plugged')

Plug 'ervandew/supertab'

Plug 'w0rp/ale'
Plug 'sbdchd/neoformat'

Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-rsi'
Plug 'jiangmiao/auto-pairs'


call plug#end()

let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1
let g:ale_sign_column_always = 1
let g:ale_sign_warning = '──'
let g:ale_sign_error = ''
let g:ale_echo_msg_warning_str = 'warning'
let g:ale_echo_msg_error_str = 'error'
let g:ale_echo_msg_format = '%severity%(%linter%): %s'

let g:SuperTabDefaultCompletionType = '<c-n>'
