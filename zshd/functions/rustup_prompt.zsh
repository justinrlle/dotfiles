#!/usr/bin/env zsh

function rustup_prompt {
  hash rustup 2> /dev/null || return
  local toolchain_info="$(rustup show | tail -3 | tr '\n' '@')"
  local toolchain_info="${toolchain_info%%@@}"
  local is_default=$([[ "${toolchain_info%%@*}" =~ .*(default) ]] && echo true || echo false)
  local toolchain_info="${toolchain_info##*@}"
  local toolchain_version=${toolchain_info%%-*}
  local toolchain_version=${toolchain_version##rustc }
  local toolchain_version=${toolchain_version%% *}
  local toolchain_type=$(
  if [[ "${toolchain_info}" =~ .*nightly.* ]]; then
    echo nightly
  elif [[ "${toolchain_info}" =~ .*beta.* ]]; then
    echo beta
  else
    echo stable
  fi
  )
  if [[ $# -lt 3 ]]; then
    echo -e "${toolchain_type}\n${toolchain_version}\n${is_default}"
  else
    eval "$1=\"$toolchain_type\""
    eval "$2=\"$toolchain_version\""
    eval "$3=\"$is_default\""
  fi
}
