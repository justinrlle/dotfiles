if len($TMUX) > 0
  autocmd BufWritePost <buffer>
        \ call system('tmux source-file ' . expand('~/.tmux.conf')) |
        \ call system('tmux display-message "Config reloaded"')
endif
