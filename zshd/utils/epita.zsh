epita_git_name='Justin Rerolle'
epita_git_email='justin.rerolle@epita.fr'

init-epiproject() {
  local project_name="$1"

  [[ ! -d '.git' ]] && git init
  git config --local user.name $epita_git_name
  git config --local user.email $epita_git_email
  echo '* justin.rerolle' > AUTHORS

  if [[ ! -z "$project_name" ]]; then
    echo "= $project_name =" | tee README > TODO
    git remote add origin "git@git.acu.epita.fr:${project_name}-2019/justin-rerolle-${project_name}.git"
    git add TODO README AUTHORS
    git commit -m 'Initial commit'
  fi
}

# debug eval calls
switch-git-user() {
  local curr_dir_hash=$(pwd | hasher)
  local name_var="\$__old_git_name_$curr_dir_hash"
  local email_var="\$__old_git_email_$curr_dir_hash"
  local old_name=$(git config user.name )
  local old_email=$(git config user.email )
  local new_name=$epita_git_name
  local new_email=$epita_git_email

  if [[ ! -z $(eval "echo $name_var") ]]; then
    if ! eval "new_name=$name_var"; then
      echo 'here 0!'
    fi
  fi
  if [[ ! -z $(eval "echo $email_var") ]]; then
    if ! eval "new_email=$email_var"; then
      echo 'here 1!'
    fi
  fi

  eval "$name_var=$old_name"
  eval "$email_var=$old_email"
  git config user.name $new_name
  git config user.email $new_email
}

hasher() {
  if type md5 > /dev/null; then
    md5 $1
  elif type md5sum > /dev/null; then
    md5sum $1 | cut -d' ' -f1
  else

  fi
}
