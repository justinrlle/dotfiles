#!/usr/bin/env zsh
function docker {
  if [ $# -eq 0 ]; then
    command docker
    return $?
  fi
  local cmd=$1; shift
  if command -v "docker_$cmd" &> /dev/null; then
    "docker_$cmd" "$@"
  else
    command docker "$cmd" "$@"
  fi
}


function docker_shell {
  local shell=$SHELL
  if [ $# -eq 2 ]; then
    shell="$2"
  fi
  if [ $# -eq 0 ] || [ $# -gt 2 ]; then
    echo "Wrong number of arguments"
    return
  fi
  command docker exec -it "$1" "$shell" -c "cd /root; $shell; exit 0"
}
