# vi: ft=zsh

if [ ! -d "$HOME/.zgen" ]; then
  git clone https://github.com/tarjoilija/zgen "$HOME/.zgen"
fi

source "$HOME/.zshd/utils/zrcautoload.zsh"

if ! vcs_info > /dev/null 2>&1; then
  zrcautoload vcs_info || vcs_info() {return 1}
fi
zmodload zsh/zpty

fpath=("$HOME/.zshd/functions" $fpath)

source "$HOME/.zgen/zgen.zsh"
if ! zgen saved; then
  zgen oh-my-zsh
  zgen oh-my-zsh plugins/git
  zgen oh-my-zsh plugins/rust
  zgen oh-my-zsh plugins/pip
  zgen oh-my-zsh plugins/yarn
  zgen oh-my-zsh plugins/macos
  zgen oh-my-zsh plugins/git-extras
  zgen oh-my-zsh plugins/git-flow
  zgen oh-my-zsh plugins/docker
  zgen oh-my-zsh plugins/colored-man-pages
  zgen oh-my-zsh plugins/sprunge
  zgen oh-my-zsh plugins/wd
  zgen oh-my-zsh plugins/mix
  zgen oh-my-zsh plugins/aws


  zgen load zsh-users/zsh-completions
  zgen load zsh-users/zsh-autosuggestions
  zgen load Tarrasch/zsh-bd
  zgen load MichaelAquilina/zsh-you-should-use
  zgen load sindresorhus/pure async.zsh main
  zgen load sindresorhus/pure pure.zsh main

  if whence fzf >/dev/null; then
     zgen load junegunn/fzf shell/completion.zsh
     zgen load junegunn/fzf shell/key-bindings.zsh
  fi

  zgen save

fi

# plugins settings
export YSU_MODE=BESTMATCH

if [ $TERM = 'xterm-termite' ]; then
  bindkey '^[[1;3C' forward-word
  bindkey '^[[1;3D' backward-word

  # means the terminal support truecolor
  export COLORTERM=truecolor
elif [ $TERM = 'xterm-kitty' ]; then
  bindkey '^[[1;3C' forward-word
  bindkey '^[[1;3D' backward-word
  bindkey '^[≥' insert-last-word
else
  bindkey '^[^[[C' forward-word
  bindkey '^[^[[D' backward-word
fi
bindkey '^U' backward-kill-line

if [[ $(uname -s) = 'Darwin' ]]; then
  export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
fi

if hash nvim 2> /dev/null; then
  local my_editor=`where nvim | head -1`
else
  local my_editor=`where vim | head -1`
fi

export VISUAL=$my_editor
export EDITOR=$VISUAL
FIGNORE=.o

alias ed="$(basename "$my_editor")"
alias l='ls -A'
unalias _ # _ as a sudo alias is unreadable
alias gt='git tree | head -$(($(tput lines) - 5))'
alias grc='git commit --amend --no-edit --date="$(date --rfc-email)"'
alias calc='noglob expr'

function tmx-reload () {
  tmux source-file ~/.tmux.conf
  tmux display-message 'Config reloaded'
}

function source-maybe () {
  if [ -r "$1" ]; then
    source "$1"
  fi
}

for file in "$HOME/.zshd/utils/"*; do
  source "$file"
done

export LOCAL_ZSHRC="${HOME}/.zshrc.${HOST}"

source-maybe "$LOCAL_ZSHRC"
source-maybe "$HOME/.zshrc.local"

# OPAM configuration
source-maybe "$HOME/.opam/opam-init/init.zsh"

# SDKMAN configuration
source-maybe "$SDKMAN_DIR/sdkman-init.sh"

# must be at the end, because it overwrites the clean function
zgen load z-shell/fast-syntax-highlighting . main

