[[ $- != *i* ]] && return

shopt -s autocd
shopt -s histappend
shopt -s cmdhist
shopt -s lithist
shopt -s checkwinsize

export HISTCONTROL=ignoreboth
export HISTSIZE=1000
export HISTFILESIZE=2000
export FIGNORE=".o"

if [ -r "$HOME/.bashrc.local" ]; then
  source "$HOME/.bashrc.local"
fi

function tmx-reload() {
  tmux source-file ~/.tmux.conf
  tmux display-message 'Config reloaded'
}

